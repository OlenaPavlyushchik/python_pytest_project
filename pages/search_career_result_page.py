from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from pages.job_detail_page import JobDetailPage


class SearchCareerResultPage:
    def __init__(self, driver):
        self.driver = driver
        self.result_list = []
        self.any_link = (By.CLASS_NAME, 'search-result__item-wrapper')
        self.view_and_apply_button = (By.LINK_TEXT, 'View and apply')

    def find_all_buttons(self):
        self.result_list = WebDriverWait(self.driver, 10).until(ec.presence_of_all_elements_located(self.any_link))
        self.result_list[-1].find_element(*self.view_and_apply_button).click()

    def click_button(self):
        WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable(self.view_and_apply_button))
        self.driver.find_element(*self.view_and_apply_button).click()

