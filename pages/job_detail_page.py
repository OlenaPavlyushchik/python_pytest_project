from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class JobDetailPage:
    def __init__(self, driver):
        self.driver = driver
        self.body = (By.TAG_NAME, 'body')

    def find_search_word_on_page(self):
        body_text = self.driver.find_element(*self.body).text
        return body_text

