from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class SearchPage:
    def __init__(self, driver):
        self.driver = driver
        self.results = []
        self.links_in_a_list = (By.CSS_SELECTOR, 'a.search-results__title-link')

    def find_all_links(self):
        self.results = WebDriverWait(self.driver, 10).until(ec.presence_of_all_elements_located(self.links_in_a_list))
        return self.results
