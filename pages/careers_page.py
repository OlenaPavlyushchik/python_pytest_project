from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from pages.search_career_result_page import SearchCareerResultPage


class CareersPage:
    def __init__(self, driver):
        self.driver = driver
        self.keyword_input_field = (By.ID, "new_form_job_search-keyword")
        self.remote_checkbox = (By.XPATH, "//label[contains(text(), 'Remote')]")
        self.find_button = (By.CSS_SELECTOR, 'button[type="submit"]')
        self.location = (By.CLASS_NAME, 'select2-search__field')
        self.location_2 = (By.CSS_SELECTOR, ".select2-selection__rendered")
        self.all_locations = (By.XPATH, "//li[text()='All Locations']")
        self.place_on_page = (By.CLASS_NAME, 'section-ui__parallax-wrapper')

    def fill_in_keyword_field(self, keyword):
        self.driver.find_element(*self.keyword_input_field).send_keys(keyword)

    def select_remote_checkbox(self):
        self.driver.find_element(*self.remote_checkbox).click()

    def click_find_button(self) -> SearchCareerResultPage:
        self.driver.find_element(*self.find_button).click()
        return SearchCareerResultPage(self.driver)
