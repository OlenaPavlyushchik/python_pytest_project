from selenium.webdriver.common.by import By
from pages.careers_page import CareersPage
from pages.search_page import SearchPage


class HomePage:
    def __init__(self, driver):
        self.driver = driver
        self.magnifying_glass = (By.CSS_SELECTOR, 'span[class="search-icon dark-iconheader-search__search-icon"]')
        self.search_fill_in = (By.ID, "new_form_search")
        self.find_button = (By.CLASS_NAME, "bth-text-layer")
        self.accept_all_button = (By.ID, "onetrust-accept-btn-handler")
        self.careers_link = (By.LINK_TEXT, 'Careers')

    def open_page(self, url):
        self.driver.get(url)

    def click_magnifying_icon(self):
        self.driver.find_element(*self.magnifying_glass).click()

    def find_search_fill_in_field(self):
        return self.driver.find_element(*self.search_fill_in)

    def fill_in_search_field(self, search_word):
        self.driver.find_element(*self.search_fill_in).send_keys(search_word)

    def click_find_button(self) -> SearchPage:
        self.driver.find_element(*self.find_button).click()
        return SearchPage(self.driver)

    def click_accept_all_button(self):
        # this did not work, looks like element Accept all was obfuscated
        # WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located(self.accept_all_button))
        # WebDriverWait(self.driver, 10).until(ec.element_to_be_clickable((By.ID, 'onetrust-accept-btn-handler')))
        # self.driver.find_element(*self.accept_all_button).click()
        accept_all_button = self.driver.find_element(By.ID, 'onetrust-accept-btn-handler')
        self.driver.execute_script("arguments[0].click();", accept_all_button)

    def click_careers_link(self) -> CareersPage:
        self.driver.find_element(*self.careers_link).click()
        return CareersPage(self.driver)


