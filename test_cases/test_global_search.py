import pytest
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from pages.home_page import HomePage
from selenium.webdriver.common.by import By
from constants import BASE_URL, SEARCH_WORD


@pytest.mark.usefixtures("driver")
def test_general_search(driver):

    home_page = HomePage(driver)

    # Step 1: Open home page and click magnifying glass icon
    home_page.open_page(BASE_URL)
    home_page.click_accept_all_button()
    home_page.click_magnifying_icon()

    # Expected result: search fill in form is opened
    WebDriverWait(driver, 10).until(ec.visibility_of_element_located((By.ID, "new_form_search")))
    assert home_page.find_search_fill_in_field().is_displayed()

    # Step 2: Fill in search word and click 'Find' button. Check that search word is present in links
    home_page.fill_in_search_field(SEARCH_WORD)
    search_page = home_page.click_find_button()
    search_page.find_all_links()
    for result in search_page.results:
        assert SEARCH_WORD in result.text.lower()
