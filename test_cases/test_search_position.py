import pytest
from pages.home_page import HomePage
from constants import BASE_URL, KEYWORD, LOCATION


@pytest.mark.usefixtures("driver")
def test_position_search(driver):
    home_page = HomePage(driver)
    home_page.open_page(BASE_URL)
    home_page.click_accept_all_button()
    careers_page = home_page.click_careers_link()
    careers_page.fill_in_keyword_field(KEYWORD)
    careers_page.select_remote_checkbox()
    search_career_result_page = careers_page.click_find_button()

    search_career_result_page.click_button()

    # job_detail_page = search_career_result_page.result_list[-1].click()
    # body_text = job_detail_page.find_search_word_on_page()
    # assert KEYWORD in body_text

