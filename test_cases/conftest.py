import pytest
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from pages.home_page import HomePage
from constants import BASE_URL, SEARCH_WORD
from selenium.webdriver.common.by import By


@pytest.fixture()
def driver():
    driver = webdriver.Chrome()
    driver.implicitly_wait(10)
    yield driver
    driver.close()
    driver.quit()


# @pytest.fixture
# def landing_page(driver):
#     home_page = HomePage(driver)
#     home_page.open_page(BASE_URL)
#     home_page.home_page_opened_check()
#     home_page.click_login_link()
#     WebDriverWait(driver, 10).until(ec.visibility_of_element_located((By.ID, "loginusername")))
#     landing_page = home_page.login(USERNAME, PASSWORD)
#     WebDriverWait(driver, 10).until(ec.visibility_of_element_located((By.ID, "logout2")))
#     landing_page.landing_page_opened()
#     assert landing_page.find_logout_button().is_displayed()
#     yield landing_page